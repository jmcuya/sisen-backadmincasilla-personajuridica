ExcelJS = require('exceljs');
recursos = require('../common/recursosConstants');
const mongodb = require('./../database/mongodb');
const appConstants = require('./../common/appConstants');
const mongoCollections = require('./../common/mongoCollections');
const logger = require('./../server/logger').logger;
const utils = require('./../common/utils');
const moment = require('moment');
const FILL_HEADER = {
    type: "pattern",
    pattern: "solid",
    bgColor: { argb: "1d357200" },
};
const ALIGMENT_CENTER = { vertical: "middle", horizontal: "center" };
const ALIGMENT_LEFT = { vertical: "middle", horizontal: "left" };
const ALIGMENT_JUNP_TEXT = { wrapText: true, vertical: 'middle', horizontal: 'center' };
const ALIGMENT_MIDDLE = { vertical: "middle" };
const ALIGMENT_JUNP_HEADER = { wrapText: true, vertical: 'middle', horizontal: 'center' };
const ALIGMENT_JUNP_CELL = { wrapText: true};

const FONT_SHEET_HEADER = {
    name: 'Calibri',
    size: 18,
    bold: true,
    color: { argb: '00000000' },
};
const FONT_ROWS = {
    name: 'Calibri',
    size: 12,
    color: { argb: '00000000' },
};
const FONT_COLUMN_HEADER = {
    name: 'Calibri',
    size: 12,
    bold: true,
    color: { argb: '00FFFFFF' },
};
const FONT_COLUMN_BOLD = {
    name: 'Calibri',
    size: 12,
    bold: true,
};
const BORDER_THIN = {
    top: { style: 'thin' },
    left: { style: 'thin' },
    bottom: { style: 'thin' },
    right: { style: 'thin' }
};

const reporteCasillas = async (usuario, fechaInicio, fechaFin) => {
    hoy = utils.getDate();
    hoyFormat = moment(hoy).add(5, 'hours').format("YYYY-MM-DD HH:mm:ss");
    //hoyFormat = hoyFormat1.tz('America/Lima').format("YYYY-MM-DD HH:mm:ss");

    const workbook = new ExcelJS.Workbook();
    workbook.lastModifiedBy = 'ONPE';
    workbook.created = hoy;
    workbook.modified = hoy;
    workbook.lastPrinted = hoy;
    const worksheet = workbook.addWorksheet('Hoja1');

    worksheet.mergeCells('C1:X3');
    worksheet.getCell('C1').value = 'Reporte de Casillas Electrónicas';
    worksheet.getCell('C1').font = FONT_SHEET_HEADER;
    worksheet.getCell('C1').alignment = ALIGMENT_CENTER;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 25;
    worksheet.getColumn(6).width = 25;
    worksheet.getColumn(7).width = 20;
    worksheet.getColumn(8).width = 20;
    worksheet.getColumn(9).width = 23;
    worksheet.getColumn(10).width = 23;
    worksheet.getColumn(11).width = 20;
    worksheet.getColumn(12).width = 30;
    worksheet.getColumn(13).width = 30;
    worksheet.getColumn(14).width = 15;
    worksheet.getColumn(15).width = 17;
    worksheet.getColumn(16).width = 22;
    worksheet.getColumn(17).width = 20;
    worksheet.getColumn(18).width = 18;
    worksheet.getColumn(19).width = 50;
    worksheet.getColumn(20).width = 50;
    worksheet.getColumn(21).width = 50;
    worksheet.getColumn(22).width = 50;
    worksheet.getColumn(23).width = 50;
    worksheet.getColumn(24).width = 150;

    const HEADER = ['C8', 'D8', 'E8', 'F8', 'G8', 'H8', 'I8', 'J8', 'K8', 'L8', 'M8', 'N8', 'O8', 'P8','Q8','R8','S8','T8','U8','V8','W8','X8'];
    for (let item of HEADER) {
        worksheet.getCell(item).fill = FILL_HEADER;
        worksheet.getCell(item).font = FONT_COLUMN_HEADER;
        worksheet.getCell(item).alignment = ALIGMENT_CENTER;
        worksheet.getCell(item).border = BORDER_THIN;
    }
    const logo = workbook.addImage({
        base64: recursos.LOGO_BASE64,
        extension: 'png',
    });
    worksheet.addImage(logo, 'A1:B3');


    worksheet.getCell('C5').value = 'Usuario: ';
    worksheet.getCell('D5').value = usuario;
    worksheet.getCell('D5').alignment = ALIGMENT_LEFT;

    worksheet.getCell('C6').value = 'Fecha: ';
    worksheet.getCell('D6').value = hoyFormat;//moment(hoy).format("DD/MM/YYYY");

    worksheet.getCell('D6').alignment = ALIGMENT_LEFT;

    worksheet.getCell('C5').font = FONT_COLUMN_BOLD;
    worksheet.getCell('C6').font = FONT_COLUMN_BOLD;

    worksheet.getCell('C8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('D8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('E8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('F8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('G8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('H8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('I8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('J8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('K8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('L8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('M8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('N8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('O8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('P8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('Q8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('R8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('S8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('T8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('U8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('V8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('W8').alignment = ALIGMENT_JUNP_HEADER;
    worksheet.getCell('X8').alignment = ALIGMENT_JUNP_HEADER;

    worksheet.getCell('C8').value = 'ÍTEM';
    worksheet.getCell('D8').value = 'FECHA DE SOLICITUD'; //'Fecha de Creación de la Casilla';
    worksheet.getCell('E8').value = 'NÚMERO DE EXPEDIENTE';// 'Titular de la Casilla';
    worksheet.getCell('F8').value = 'CANAL DE PRESENTACIÓN'; //'Institución / Organización Política';
    worksheet.getCell('G8').value = 'TIPO DE DOCUMENTO'; //'Usuario';
    worksheet.getCell('H8').value = 'NÚMERO DE DOCUMENTO'; //Fecha Presentacion';
    worksheet.getCell('I8').value = 'NOMBRES'; //'Num Expediente';
    worksheet.getCell('J8').value = 'APELLIDO PATERNO'; //'Num Expediente';
    worksheet.getCell('K8').value = 'APELLIDO MATERNO'; //'Num Expediente';
    worksheet.getCell('L8').value = 'DOMICILIO FÍSICO';
    worksheet.getCell('M8').value = 'DEPARTAMENTO';
    worksheet.getCell('N8').value = 'PROVINCIA';
    worksheet.getCell('O8').value = 'DISTRITO';
    worksheet.getCell('P8').value = 'CORREO ELECTRÓNICO';
    worksheet.getCell('Q8').value = 'TELÉFONO FIJO';
    worksheet.getCell('R8').value = 'CELULAR';
    worksheet.getCell('S8').value = 'CONTRASEÑA ACTUALIZADA';
    worksheet.getCell('T8').value = 'FECHA DE CREACIÓN / EVALUACIÓN';
    worksheet.getCell('U8').value = 'PENDIENTE A LA FECHA';
    worksheet.getCell('V8').value = 'USUARIO CREADOR';
    worksheet.getCell('W8').value = 'ESTADO';
    worksheet.getCell('X8').value = 'MOTIVO OBSERVACIÓN';
    


    let i = 1;
    let j = 9;
    let filter = {
        //con let cursos original // profile: appConstants.PROFILE_CITIZEN,
    }

    if (fechaInicio && fechaFin) {
        fechaFin = new Date(fechaFin+"T04:59:59.000Z");
        fechaFin.setDate(fechaFin.getDate() + 1)
        let rango = { $gte: new Date(fechaInicio+"T04:59:59.000Z"), $lte: fechaFin };
        filter = { ...filter, $or: [{evaluated_at : rango},  {update_date: rango}] }
    } else if (fechaInicio) {
        let rango = { $gte: new Date(fechaInicio+"T04:59:59.000Z") };
        filter = { ...filter, $or: [{evaluated_at : rango},  {update_date: rango}] }
    } else if (fechaFin) {
        fechaFin = new Date(fechaFin);
        fechaFin.setDate(fechaFin.getDate() + 1)
        let rango = { $lte: new Date(fechaFin+"T04:59:59.000Z") };
        filter = { ...filter, $or: [{evaluated_at : rango},  {update_date: rango}] }
    }
    const db = await mongodb.getDb();
    //let cursor = await db.collection(mongoCollections.USERS).find(filter).collation({ locale: "en", strength: 1 }).sort({ created_at: -1 });
    //ver2 let cursor = await db.collection(mongoCollections.INBOX).aggregate([{$match:filter}, {$lookup:{from:'users',localField:'doc',foreignField:'doc',as:'user'}}, {$sort:{created_at: -1}}],{$collation:{locale:"en", strength:1}});

    console.log('date init: '+ new Date());

    let cursor = await db.collection(mongoCollections.INBOX).aggregate([
                {$match:filter},
                {$sort:{ evaluated_at: 1, update_date: 1}},
                {$lookup:{
                        "from":"users",
                        "foreignField":"_id",
                        "localField":"user_id",
                        "as":"user"
                    }},
                {$project: {_id: 0,
                        created_at: 1,
                        nroExpediente: 1,
                        create_user: 1,
                        doc_type: 1,
                        doc: 1,
                        address: 1,
                        email: 1,
                        cellphone: 1,
                        phone:1,
                        status: 1,
                        motivo: 1,
                        update_date:1,
                        evaluator_user_names: 1,
                        dateFiling: 1,
                        'user.Ubigeo': 1,
                        'user.name': 1,
                        'user.lastname': 1,
                        'user.second_lastname': 1,
                        'user.updated_password': 1,
                }},
            ],
            {
                allowDiskUse: true
            });


    console.log('date fin: '+ new Date());

    for await (const item of cursor) {
        // console.log("item: " +item);
        var motivos = "";
        if(item.motivo !== undefined){
//            console.log("motivo: " +`${item.motivo.motivo1 != undefined ? item.motivo.motivo1.detalle : ""}`);
            if(item.motivo.motivo1 != undefined && item.motivo.motivo1.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo1.detalle };
            if(item.motivo.motivo2 != undefined && item.motivo.motivo2.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo2.detalle };
            if(item.motivo.motivo3 != undefined && item.motivo.motivo3.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo3.detalle };
            if(item.motivo.motivo4 != undefined && item.motivo.motivo4.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo4.detalle };
            if(item.motivo.motivo5 != undefined && item.motivo.motivo5.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo5.detalle };
            if(item.motivo.motivo6 != undefined && item.motivo.motivo6.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo6.detalle };
            if(item.motivo.motivo7 != undefined && item.motivo.motivo7.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo7.detalle };
            if(item.motivo.motivo8 != undefined && item.motivo.motivo8.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo8.detalle };
            if(item.motivo.motivo9 != undefined && item.motivo.motivo9.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo9.detalle };
            if(item.motivo.motivo10 != undefined && item.motivo.motivo10.value) {motivos = motivos + `${motivos.length>0?"\n":""}` + item.motivo.motivo10.detalle };
        }
        var tmpDep="", tmpPro="", tmpDis="";
        if(item.user.length>0){
        if(item.user[0].Ubigeo!=undefined){
            var separado = item.user[0].Ubigeo.split("/");
            tmpDep = separado[0];
            tmpPro = separado[1];
            tmpDis = separado[2];
        }
        }
        var tmpFechaCreatedAt = "";
        if(item.create_user != undefined && item.create_user=="owner") {
            tmpFechaCreatedAt = moment(item.created_at).format("YYYY-MM-DD HH:mm:ss");
        } else {
            tmpFechaCreatedAt = item.dateFiling != undefined?moment(item.dateFiling).format("YYYY-MM-DD"):"";
        }
        worksheet.getCell(`C${j}`).value = i;
        worksheet.getCell(`D${j}`).value = tmpFechaCreatedAt; //`${item.created_at != undefined ? moment(item.created_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;
        worksheet.getCell(`E${j}`).value = item.nroExpediente; //`${item.user[0].name} ${item.user[0].lastname} ${item.user[0].second_lastname != undefined ? item.user[0].second_lastname : ''}`;
        worksheet.getCell(`F${j}`).value = `${item.create_user=='owner' ? 'SISEN':'MP'}`; //item.user[0].organization_name;
        worksheet.getCell(`G${j}`).value = item.doc_type.toUpperCase(); //item.doc;
        worksheet.getCell(`H${j}`).value = item.doc; //item.dateFiling;
        worksheet.getCell(`I${j}`).value = `${item.user.length > 0 ? item.user[0].name : ''}`; //item.nroExpediente;
        worksheet.getCell(`J${j}`).value = `${item.user.length > 0 ? item.user[0].lastname : ''}`; //item.nroExpediente;
        worksheet.getCell(`K${j}`).value = `${item.user.length > 0 && item.user[0].second_lastname != undefined ? item.user[0].second_lastname : ''}`; //item.nroExpediente;
        worksheet.getCell(`L${j}`).value = item.address;
        worksheet.getCell(`M${j}`).value = tmpDep;//DEPARTAMENTO
        worksheet.getCell(`N${j}`).value = tmpPro;//PROVINCIA
        worksheet.getCell(`O${j}`).value = tmpDis;//DISTRITO
        worksheet.getCell(`P${j}`).value = item.email;
        worksheet.getCell(`Q${j}`).value = `${item.phone != "undefined"? item.phone !== null? item.phone:"":""}`;//TELEFONO FIJO
        worksheet.getCell(`R${j}`).value = item.cellphone;
        worksheet.getCell(`S${j}`).value = `${item.user.length > 0 ? item.user[0].updated_password==true?"VERDADERO":"FALSO" : ''}`;
        worksheet.getCell(`T${j}`).value = `${item.evaluated_at != undefined? moment(item.evaluated_at).format("YYYY-MM-DD HH:mm:ss") : moment(item.update_date).format("YYYY-MM-DD HH:mm:ss") }`; //moment(item.created_at).format("YYYY-MM-DD HH:mm:ss")
        worksheet.getCell(`U${j}`).value = `${item.status != undefined ? item.status =="PENDIENTE" ? hoyFormat:"" : ''}`;
        worksheet.getCell(`V${j}`).value = `${item.create_user != 'owner' ? item.create_user:(item.evaluator_user_names != undefined? item.evaluator_user_names:'')}`; //`${item.user_register.length > 0 ? item.user_register[0].name+" "+item.user_register[0].lastname+" "+item.user_register[0].second_lastname : ''}` ;
        worksheet.getCell(`W${j}`).value = `${item.status != undefined ? item.status : "REGISTRO INTERNO"}`;
        worksheet.getCell(`X${j}`).value = motivos; //item.motivo;

        worksheet.getCell(`C${j}`).border = BORDER_THIN;
        worksheet.getCell(`D${j}`).border = BORDER_THIN;
        worksheet.getCell(`E${j}`).border = BORDER_THIN;
        worksheet.getCell(`F${j}`).border = BORDER_THIN;
        worksheet.getCell(`G${j}`).border = BORDER_THIN;
        worksheet.getCell(`H${j}`).border = BORDER_THIN;
        worksheet.getCell(`I${j}`).border = BORDER_THIN;
        worksheet.getCell(`J${j}`).border = BORDER_THIN;
        worksheet.getCell(`K${j}`).border = BORDER_THIN;
        worksheet.getCell(`L${j}`).border = BORDER_THIN;
        worksheet.getCell(`M${j}`).border = BORDER_THIN;
        worksheet.getCell(`N${j}`).border = BORDER_THIN;
        worksheet.getCell(`O${j}`).border = BORDER_THIN;
        worksheet.getCell(`P${j}`).border = BORDER_THIN;
        worksheet.getCell(`Q${j}`).border = BORDER_THIN;
        worksheet.getCell(`R${j}`).border = BORDER_THIN;
        worksheet.getCell(`S${j}`).border = BORDER_THIN;
        worksheet.getCell(`T${j}`).border = BORDER_THIN;
        worksheet.getCell(`U${j}`).border = BORDER_THIN;
        worksheet.getCell(`V${j}`).border = BORDER_THIN;
        worksheet.getCell(`W${j}`).border = BORDER_THIN;
        worksheet.getCell(`X${j}`).border = BORDER_THIN;

        worksheet.getCell(`C${j}`).font = FONT_ROWS;
        worksheet.getCell(`D${j}`).font = FONT_ROWS;
        worksheet.getCell(`E${j}`).font = FONT_ROWS;
        worksheet.getCell(`F${j}`).font = FONT_ROWS;
        worksheet.getCell(`G${j}`).font = FONT_ROWS;
        worksheet.getCell(`H${j}`).font = FONT_ROWS;
        worksheet.getCell(`I${j}`).font = FONT_ROWS;
        worksheet.getCell(`J${j}`).font = FONT_ROWS;
        worksheet.getCell(`K${j}`).font = FONT_ROWS;
        worksheet.getCell(`L${j}`).font = FONT_ROWS;
        worksheet.getCell(`M${j}`).font = FONT_ROWS;
        worksheet.getCell(`N${j}`).font = FONT_ROWS;
        worksheet.getCell(`O${j}`).font = FONT_ROWS;
        worksheet.getCell(`P${j}`).font = FONT_ROWS;
        worksheet.getCell(`Q${j}`).font = FONT_ROWS;
        worksheet.getCell(`R${j}`).font = FONT_ROWS;
        worksheet.getCell(`S${j}`).font = FONT_ROWS;
        worksheet.getCell(`T${j}`).font = FONT_ROWS;
        worksheet.getCell(`U${j}`).font = FONT_ROWS;
        worksheet.getCell(`V${j}`).font = FONT_ROWS;
        worksheet.getCell(`W${j}`).font = FONT_ROWS;
        worksheet.getCell(`X${j}`).font = FONT_ROWS;

        worksheet.getCell(`C${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`D${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`E${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`F${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`G${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`H${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`I${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`J${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`K${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`L${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`M${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`N${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`O${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`P${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`Q${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`R${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`S${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`T${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`U${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`V${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`W${j}`).alignment = ALIGMENT_MIDDLE;
        worksheet.getCell(`X${j}`).alignment = ALIGMENT_JUNP_CELL;

        j++;
        i++;
    }
    const buffer = await workbook.xlsx.writeBuffer();
    return buffer;
}
const reporteNotificaciones = async (usuario, fechaInicio, fechaFin) => {
    hoy = utils.getDate();

    const workbook = new ExcelJS.Workbook();
    workbook.lastModifiedBy = 'ONPE';
    workbook.created = hoy;
    workbook.modified = hoy;
    workbook.lastPrinted = hoy;
    const worksheet = workbook.addWorksheet('Hoja1');

    worksheet.mergeCells('C1:Q3');
    worksheet.getCell('C1').value = 'Reporte de Notificaciones';
    worksheet.getCell('C1').font = FONT_SHEET_HEADER;
    worksheet.getCell('C1').alignment = ALIGMENT_CENTER;
    worksheet.getColumn(3).width = 10;
    worksheet.getColumn(4).width = 35;
    worksheet.getColumn(5).width = 100;
    worksheet.getColumn(6).width = 50;
    worksheet.getColumn(7).width = 100;
    worksheet.getColumn(8).width = 40;
    worksheet.getColumn(9).width = 60;
    worksheet.getColumn(10).width = 20;
    worksheet.getColumn(11).width = 20;
    worksheet.getColumn(12).width = 20;
    worksheet.getColumn(13).width = 50;
    worksheet.getColumn(14).width = 50;
    worksheet.getColumn(15).width = 50;
    worksheet.getColumn(16).width = 50;
    worksheet.getColumn(17).width = 50;

    const HEADER = ['C8', 'D8', 'E8', 'F8', 'G8', 'H8', 'I8', 'J8','K8','L8','M8','N8','O8'];
    for (let item of HEADER) {
        worksheet.getCell(item).fill = FILL_HEADER;
        worksheet.getCell(item).font = FONT_COLUMN_HEADER;
        worksheet.getCell(item).alignment = ALIGMENT_CENTER;
        worksheet.getCell(item).border = BORDER_THIN;
    }
    const logo = workbook.addImage({
        base64: recursos.LOGO_BASE64,
        extension: 'png',
    });
    worksheet.addImage(logo, 'A1:B3');
    worksheet.getCell('C5').value = 'Usuario: ';
    worksheet.getCell('D5').value = usuario;
    worksheet.getCell('D5').alignment = ALIGMENT_LEFT;

    worksheet.getCell('C6').value = 'Fecha: ';
    worksheet.getCell('D6').value = moment(hoy).format("DD/MM/YYYY");
    worksheet.getCell('D6').alignment = ALIGMENT_LEFT;

    worksheet.getCell('C5').font = FONT_COLUMN_BOLD;
    worksheet.getCell('C6').font = FONT_COLUMN_BOLD;

    worksheet.getCell('C8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('D8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('E8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('F8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('G8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('H8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('I8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('J8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('K8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('L8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('M8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('N8').alignment = ALIGMENT_JUNP_TEXT;
    worksheet.getCell('O8').alignment = ALIGMENT_JUNP_TEXT;

    worksheet.getCell('C8').value = 'ÍTEM';
    worksheet.getCell('D8').value = 'FECHA DE CREACIÓN DE LA NOTIFICACIÓN'; // 'FECHA DE CARGA AL SISEN'; //Nombre del Notificador';
    worksheet.getCell('E8').value = 'EXPEDIENTE';//'DOCUMENTO N° (CON TODA SU NOMENCLATURA)'; //'Fecha de Notificación';
    worksheet.getCell('F8').value = 'TIPO DE DOCUMENTO'; //'NOMBRES Y APELLIDOS'; //'UU.OO Emisora';
    worksheet.getCell('G8').value = 'NÚMERO DE DOCUMENTO'; // 'ASUNTO'; //'Documento';
    worksheet.getCell('H8').value = 'NOMBRES Y APELLIDOS'; //'FECHA DE DEPÓSITO EN CASILLA ELECTRÓNICA (NOTIFICACIÓN)'; // 'Destinatario';
    worksheet.getCell('I8').value = 'ASUNTO'; 'FECHA DE RECEPCIÓN';
    worksheet.getCell('J8').value = 'FECHA DE ENVÍO DE LA NOTIFICACIÓN';// 'ESTADO (LEÍDO / NO LEÍDO)';// 'Institución / Organización Política';
    worksheet.getCell('K8').value = 'FECHA DE RECEPCIÓN DE LA NOTIFICACIÓN';// 'FECHA DE LECTURA'; //'Fecha de lectura';
    worksheet.getCell('L8').value = 'ESTADO (LEÍDO / NO LEÍDO)';// 'PROCESO';
    worksheet.getCell('M8').value = 'FECHA DE LECTURA DE LA NOTIFICACIÓN'; // 'RESPONSABLE DE LA NOTIFICACIÓN';
    worksheet.getCell('N8').value = 'PROCESO';// 'RESPONSABLE DE LA NOTIFICACIÓN';
    worksheet.getCell('O8').value = 'RESPONSABLE DE LA NOTIFICACIÓN';

    let i = 1;
    let j = 9;
    let filter = {
    }

    if (fechaInicio && fechaFin) {
        fechaFin = new Date(fechaFin+"T04:59:59.000Z");
        fechaFin.setDate(fechaFin.getDate() + 1)
        filter = { ...filter, received_at: { $gte: new Date(fechaInicio+"T04:59:59.000Z"), $lte: new Date(fechaFin) } }
    } else if (fechaInicio) {
        filter = { ...filter, received_at: { $gte: new Date(fechaInicio+"T04:59:59.000Z") } }
    } else if (fechaFin) {
        fechaFin = new Date(fechaFin);
        fechaFin.setDate(fechaFin.getDate() + 1)
        filter = { ...filter, received_at: { $lte: new Date(fechaFin+"T04:59:59.000Z") } }
    }
    //console.log(filter);
    const db = await mongodb.getDb();
    //let cursor = await db.collection(mongoCollections.NOTIFICATIONS).find(filter).collation({ locale: "en", strength: 1 }).sort({ received_at: -1 });
    //console.log('date init: '+ new Date());
    let cursor = await db.collection(mongoCollections.NOTIFICATIONS)
        .find(filter, {_id: 0,
            created_at: 1,
            expedient: 1,
            inbox_doc:1,
            inbox_doc_type:1,
            inbox_name: 1,
            message: 1,
            sent_at: 1,
            received_at: 1,
            read_at: 1,
            procedure:1,
            'acuse_data.notifier_name': 1
        })
        .sort({ received_at: -1 });
    //console.log('date end: '+ new Date());
    
    for await (const item of cursor) {
        worksheet.getCell(`C${j}`).value = i;
        worksheet.getCell(`D${j}`).value = `${item.created_at != undefined ? moment(item.created_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;
        worksheet.getCell(`E${j}`).value = item.expedient; //item.received_at;
        worksheet.getCell(`F${j}`).value = item.inbox_doc_type.toUpperCase(); //item.inbox_name; //item.uuooName;
        worksheet.getCell(`G${j}`).value = item.inbox_doc;//item.message; //item.inbox_doc;
        worksheet.getCell(`H${j}`).value = item.inbox_name;//`${item.sent_at != undefined ? moment(item.sent_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;
        worksheet.getCell(`I${j}`).value = item.message; //`${item.received_at != undefined ? moment(item.received_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;
        worksheet.getCell(`J${j}`).value = `${item.sent_at != undefined ? moment(item.sent_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;//`${item.read_at != undefined ? "LEÍDO" : "NO LEÍDO"}`; //item.organization_name;
        worksheet.getCell(`K${j}`).value = `${item.received_at != undefined ? moment(item.received_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;//`${item.read_at != undefined ? moment(item.read_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;
        worksheet.getCell(`L${j}`).value = `${item.read_at != undefined ? "LEÍDO" : "NO LEÍDO"}`; //item.organization_name;//"Proceso Electoral";
        worksheet.getCell(`M${j}`).value = `${item.read_at != undefined ? moment(item.read_at).format("YYYY-MM-DD HH:mm:ss") : ""}`;//`${item.acuse_data != undefined ? item.acuse_data.notifier_name : ''}`;
        worksheet.getCell(`N${j}`).value = item.procedure;
        worksheet.getCell(`O${j}`).value = `${item.acuse_data != undefined ? item.acuse_data.notifier_name : ''}`;

        worksheet.getCell(`C${j}`).border = BORDER_THIN;
        worksheet.getCell(`D${j}`).border = BORDER_THIN;
        worksheet.getCell(`E${j}`).border = BORDER_THIN;
        worksheet.getCell(`F${j}`).border = BORDER_THIN;
        worksheet.getCell(`G${j}`).border = BORDER_THIN;
        worksheet.getCell(`H${j}`).border = BORDER_THIN;
        worksheet.getCell(`I${j}`).border = BORDER_THIN;
        worksheet.getCell(`J${j}`).border = BORDER_THIN;
        worksheet.getCell(`K${j}`).border = BORDER_THIN;
        worksheet.getCell(`L${j}`).border = BORDER_THIN;
        worksheet.getCell(`M${j}`).border = BORDER_THIN;
        worksheet.getCell(`N${j}`).border = BORDER_THIN;
        worksheet.getCell(`O${j}`).border = BORDER_THIN;

        worksheet.getCell(`C${j}`).font = FONT_ROWS;
        worksheet.getCell(`D${j}`).font = FONT_ROWS;
        worksheet.getCell(`E${j}`).font = FONT_ROWS;
        worksheet.getCell(`F${j}`).font = FONT_ROWS;
        worksheet.getCell(`G${j}`).font = FONT_ROWS;
        worksheet.getCell(`H${j}`).font = FONT_ROWS;
        worksheet.getCell(`I${j}`).font = FONT_ROWS;
        worksheet.getCell(`J${j}`).font = FONT_ROWS;
        worksheet.getCell(`K${j}`).font = FONT_ROWS;
        worksheet.getCell(`L${j}`).font = FONT_ROWS;
        worksheet.getCell(`M${j}`).font = FONT_ROWS;
        worksheet.getCell(`N${j}`).font = FONT_ROWS;
        worksheet.getCell(`O${j}`).font = FONT_ROWS;
        j++;
        i++;
    }
    const buffer = await workbook.xlsx.writeBuffer();
    return buffer;

}

const getdatos = async (usuario, fechaInicio, fechaFin) => {
    const db = await mongodb.getDb();
    let cursor = await db.collection(mongoCollections.INBOX).find({_id:Object('6060dbfba67e0508ac9791da')});
    cursor.forEach(
        function(doc) {
            console.log(doc);
        },
        /*function(err) {
            client.close();
        }*/
    );
    //db.collection(mongoCollections.INBOX).update({})


    /*await db.collection(mongoCollections.CATALOG).update(filter, {
        $set: {
            value,
            update_user: usuarioRegistro,
            update_date: utils.getDate(),
        }
    });*/
}
module.exports = { reporteCasillas, reporteNotificaciones, getdatos };
