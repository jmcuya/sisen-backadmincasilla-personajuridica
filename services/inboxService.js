/**
 * Created by Angel Quispe
 */
const mongodb = require('./../database/mongodb');
const logger = require('./../server/logger').logger;
const mongoCollections = require('./../common/mongoCollections');
const errors = require('./../common/errors');
const appConstants = require('./../common/appConstants');
const ObjectId = require('mongodb').ObjectId;
const fs = require('fs');
const representativeService = require('./../services/representativeService');

const base_url = process.env.BASE_URL;
const path_upload = process.env.PATH_UPLOAD;

const getInbox = async (docType, doc) => {
    try {
        const db = await mongodb.getDb();

        let _filter = {
            doc: doc,
            doc_type: docType
        }

        let inbox = await db.collection(mongoCollections.INBOX).findOne(_filter);

        if (!inbox) {
            logger.error('inbox ' + doc + '/' + docType + ' not exist');
            return {success: false};
        }

        return {success: true, inbox: {_id: inbox._id, doc: inbox.doc, doc_type: inbox.doc_type}};

    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }

}

const getApprovedInboxByDoc = async (docType, doc) => {
    try {
        const ESTADO_APROBADO = 'APROBADO';
        const db = await mongodb.getDb();

        let _filter = {
            $or: [
                {
                    doc: doc,
                    doc_type: docType,
                    status: ESTADO_APROBADO,
                },
                {
                    doc: doc,
                    doc_type: docType,
                    status: null,
                }
            ]
        }

        let inbox = await db.collection(mongoCollections.INBOX).findOne(_filter);

        if (!inbox) {
            logger.warn('inbox ' + doc + '/' + docType + ' approved not exist');
            return {success: false};
        }

        return {success: true, inbox: {_id: inbox._id, doc: inbox.doc, doc_type: inbox.doc_type}};

    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }
}

const getApprovedInboxByEmail = async (correo) => {
    try {
        const ESTADO_APROBADO = 'APROBADO';
        const db = await mongodb.getDb();

        let _filter = {
            $or: [
                {
                    email: correo,
                    status: ESTADO_APROBADO,
                },
                {
                    email: correo,
                    status: null,
                }
            ]
        }

        let inbox = await db.collection(mongoCollections.INBOX).findOne(_filter);

        if (!inbox) {
            logger.error('inbox with email ' + correo + ' approved not exist');
            return {success: false};
        }

        return {success: true};

    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }
}

const getInboxUserCitizen = async (docType, doc, jwt) => {
    try {
        const db = await mongodb.getDb();

        let _filter = {
            doc: doc,
            doc_type: docType
        }

        let inbox = await db.collection(mongoCollections.INBOX).findOne(_filter);

        if (!inbox) {
            logger.error('inbox ' + doc + '/' + docType + ' not exist');
            return {success: false};
        }

        return {
            success: true,
            inbox: {
                doc: inbox.doc,
                doc_type: inbox.doc_type,
                email: inbox.email,
                cellphone: inbox.cellphone,
                phone: inbox.phone,
                address: inbox.address,
                acreditation_type: inbox.acreditation_type,
                pdf_resolution: pdfBox(inbox._id, appConstants.BOX_PDF_RESOLUTION, inbox.pdf_resolution, jwt),
                pdf_creation_solicitude: pdfBox(inbox._id, appConstants.BOX_PDF_CREATION_SOLICITUDE, inbox.pdf_creation_solicitude, jwt),
                pdf_agree_tos: pdfBox(inbox._id, appConstants.BOX_PDF_AGREE_TOS, inbox.pdf_agree_tos, jwt)
            }
        };

    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }

}

const pdfBox = (idInbox, pdf_type, pdf_inbox, jwt) => {
    return {
        name: pdf_inbox.name,
        url: encodeURI(base_url + '/download-pdf-box?token=' + jwt + '&inbox=' + idInbox + '&type=' + pdf_type)
    }
}

const downloadPdfInbox = async (idInbox, pdf_type) => {
    try {
        const db = await mongodb.getDb();

        let filter = {
            _id: ObjectId(idInbox)
        }

        let inbox = await db.collection(mongoCollections.INBOX).findOne(filter);

        if (!inbox) {
            logger.error('inbox ' + idInbox + ' not exist');
            return {success: false};
        }

        let result = {success: false};

        if (fs.existsSync(path_upload + '/' + inbox[pdf_type].path)) {
            result.success = true;
            result.pathfile = path_upload + '/' + inbox[pdf_type].path;
            result.filename = inbox[pdf_type].name;
        } else {
            logger.error(pdf_type + ' - ' + path_upload + '/' + inbox[pdf_type].path + ' not exist');
        }

        return result;

    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }
}

const inboxEdit = async (data, userLogged) => {
    let user;
    let rep = {};
    let dataSetInbox = {
        email: data.email,
        address: data.address,
        cellphone: data.cellphone,
        phone: data.phone,
        update_user: userLogged,
        update_date: new Date(),
    };

    let dataSetUser = {
        email: data.email,
        address: data.address,
        cellphone: data.cellphone,
        phone: data.phone,
        Ubigeo: data.ubigeo,
        update_user: userLogged,
        update_date: new Date(),
        PaginaWeb: data.webSite,
    };

    try {
        const db = await mongodb.getDb();
        const oInbox = await db.collection(mongoCollections.INBOX).findOne({
            user_id: ObjectId(data.userId)
        });

        if (!oInbox) {
            return {success: false, error: 'No existe la casilla'};
        }

        const users = await db.collection(mongoCollections.USERS).find({_id: ObjectId(data.userId)}).toArray();
        if (users.length === 1) {
            user = users[0];
        }

        if (!user) {
            return {success: false, error: 'No es posible determinar el usuario para la casilla seleccionada'};
        }

        if (data.personType === appConstants.PERSON_TYPE_PJ) {
            rep = data.rep;

            //update representative
            const updateRep = await representativeService.update(rep);

            if (!updateRep.success) {
                return {
                    success: false,
                    error: 'No es posible determinar el representante para la casilla seleccionada'
                };
            }
        }

        const resultUpdateInbox = await db.collection(mongoCollections.INBOX).updateOne({_id: oInbox._id}, {
            $set: dataSetInbox
        });

        const resultUpdateUsers = await db.collection(mongoCollections.USERS).updateOne({_id: user._id}, {
            $set: dataSetUser
        });

        return {success: true};
    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }
}

const getInboxXNroexpediente = async (nroExpediente) => {
    try {
        const db = await mongodb.getDb();

        let _filter = {
            nroExpediente: nroExpediente
        }

        let inbox = await db.collection(mongoCollections.INBOX).findOne(_filter);

        if (!inbox) {
            logger.error('inbox ' + nroExpediente + ' not exist');
            return {success: false};
        }

        return {success: true, inbox: {_id: inbox._id, nroExpediente: inbox.nroExpediente}};

    } catch (err) {
        logger.error(err);
        return {success: false, error: errors.INTERNAL_ERROR};
    }

}

module.exports = {
    getInbox,
    getInboxUserCitizen,
    downloadPdfInbox,
    getApprovedInboxByDoc,
    getApprovedInboxByEmail,
    inboxEdit,
    getInboxXNroexpediente
};
