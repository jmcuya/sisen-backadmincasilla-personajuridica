const mongodb = require('./../database/mongodb');
const logger = require('./../server/logger').logger;
const mongoCollections = require('./../common/mongoCollections');
const {ObjectId, ObjectID} = require("mongodb");


const findByUserId = async (userId, enabled) => {

    try {
        const db = await mongodb.getDb();

        const oRep = await db.collection(mongoCollections.REPRESENTATIVE).findOne({
            user_id: ObjectId(userId),
            enabled: enabled
        }, {
            projection: {
                _id: 1,
                doc_type: 1,
                doc: 1,
                names: 1,
                lastname: 1,
                second_lastname: 1,
                cellphone: 1,
                email: 1,
                ubigeo: 1,
                position: 1,
                position_name: 1,
                file_document: 1,
                file_photo: 1,
                address: 1,
                document_type_attachment: 1,
                document_name_attachment: 1,
                phone: 1,
            }
        });

        if (!oRep) {
            return {success: false};
        }

        return {success: true, data: oRep};
    } catch (err) {
        logger.error(err);
        return {success: false};
    }
}

const save = async (data, listFile, isCreate) => {
    let user = {};

    try {
        const db = await mongodb.getDb();

        let createdAt = new Date();

        const newData = {
            doc_type: data.docType,
            doc: data.doc,
            names: data.names,
            lastname: data.lastname,
            second_lastname: data.second_lastname,
            email: data.email,
            cellphone: data.cellphone,
            phone: data.phone,
            ubigeo: data.ubigeo,
            address: data.address,
            position: data.position,
            position_name: data.positionName,
            document_type_attachment: data.documentTypeAttachment,
            document_name_attachment: data.documentNameAttachment,
            file_document: listFile,
            file_photo: null,
            enabled: true,
            created_at: createdAt,
            date_begin: createdAt,
            user_id: data.user_id,
            inbox_id: data.inbox_id,
        };

        if (!isCreate) {
            const oInbox = await db.collection(mongoCollections.INBOX).findOne({
                user_id: ObjectId(data.userId)
            });

            if (!oInbox) {
                return {success: false};
            }

            const users = await db.collection(mongoCollections.USERS).find({_id: ObjectId(data.userId)}).toArray();
            if (users.length === 1) {
                user = users[0];
            }

            if (user) {
                newData.user_id = ObjectId(data.userId);
                newData.inbox_id = oInbox._id;
            }

            const listRep = await db.collection(mongoCollections.REPRESENTATIVE).find({
                user_id: newData.user_id,
                inbox_id: newData.inbox_id
            }).toArray();

            for (let rep of listRep) {
                if (!rep.date_end) {
                    rep.date_end = createdAt;
                }

                rep.enabled = false;

                await db.collection(mongoCollections.REPRESENTATIVE).updateOne({_id: rep._id}, {
                    $set: rep
                });
            }
        }

        let result = await db.collection(mongoCollections.REPRESENTATIVE).insertOne(newData)

        return {success: true};
    } catch (err) {
        logger.error(err);
        return {success: false};
    }
}

const update = async (data) => {
    const db = await mongodb.getDb();

    try {
        const oRep = await db.collection(mongoCollections.REPRESENTATIVE).findOne({
            _id: ObjectId(data.id)
        });

        if (!oRep) {
            return {success: false};
        }

        const result = await db.collection(mongoCollections.REPRESENTATIVE).updateOne({_id: ObjectId(data.id)}, {
            $set: {
                email: data.email,
                phone: data.phone,
                cellphone: data.cellphone,
                address: data.address,
                ubigeo: data.ubigeo,
                position: data.position,
                position_name: data.positionName,
                updated_at: new Date(),
            }
        });

        if (result.modifiedCount === 1) {
            return {success: true};
        }
    } catch (err) {
        logger.error(err);
    }

    return {success: false};
}

const updateApprove = async (userId, userLogged) => {
    const db = await mongodb.getDb();
    const updatedAt = new Date();

    try {
        const evaluatorNames = userLogged.name + ' ' + userLogged.lastname + ' ' + (userLogged.second_lastname !== undefined ? userLogged.second_lastname : '');

        const resultUpdateInbox = await db.collection(mongoCollections.REPRESENTATIVE).updateOne({user_id: ObjectId(userId)}, {
            $set: {
                enabled: true,
                date_begin: updatedAt,
                updated_at: updatedAt,
                evaluator_user_id: ObjectId(userLogged.id),
                evaluator_user_names: evaluatorNames.trim(),
                evaluated_at: updatedAt
            }
        });

        return {success: true};
    } catch (err) {
        logger.error(err);
        return {success: false};
    }
}

module.exports = {findByUserId, save, update, updateApprove};
