/**
 * Created by Angel Quispe
 */
const utils = require('./../common/utils');
const inboxService = require('./../services/inboxService');
const logger = require('./../server/logger').logger;
const appConstants = require('./../common/appConstants');


const inboxEdit = async (req, res, next) => {
    let payload = req.body;
    let rep = payload.rep;
    let userLogged = req.user.name + ' ' + req.user.lastname;

    try {
        if (utils.isEmpty(payload.personType) ||
            utils.isEmpty(payload.userId) ||
            utils.isEmpty(payload.cellphone) ||
            utils.isEmpty(payload.ubigeo) ||
            utils.isEmpty(payload.address) ||
            utils.isEmpty(payload.email)) {
            return res.sendStatus(400);
        }

        if (payload.personType === appConstants.PERSON_TYPE_PJ) {
            if (utils.isEmpty(rep.id) ||
                utils.isEmpty(rep.email) ||
                utils.isEmpty(rep.cellphone) ||
                utils.isEmpty(rep.ubigeo) ||
                utils.isEmpty(rep.address)) {
                return res.sendStatus(400);
            }

            if (!utils.isEmpty(rep.phone) && rep.phone.length < 7) {
                return res.status(400).json({success: false, error: "Teléfono fijo no válido"});
            }
        }

        if (!utils.isEmpty(payload.phone) && payload.phone.length < 7) {
            return res.status(400).json({success: false, error: "Teléfono fijo no válido"});
        }

        let result = await inboxService.inboxEdit(payload, userLogged);
        return res.json(result);
    } catch (ex) {
        logger.error(ex);
        next({success: false, error: 'error'});
    }
}

module.exports = {
    inboxEdit
};
