const representativeService = require('./../services/representativeService');
const utils = require('./../common/utils');
const appConstants = require('./../common/appConstants');
const fs = require("fs");

const typeFiles = ["application/pdf", "image/jpg", "image/jpeg", "image/png", "image/bmp", "image/x-ms-bmp"];

const save = async (req, res) => {
    let data = req.fields;
    let files = req.files;

    if (
        utils.isEmpty(data.docType) ||
        utils.isEmpty(data.doc) ||
        utils.isEmpty(data.names) ||
        //utils.isEmpty(data.lastname) ||
        //utils.isEmpty(data.second_lastname) ||
        utils.isEmpty(data.email) ||
        utils.isEmpty(data.cellphone) ||
        utils.isEmpty(data.ubigeo) ||
        utils.isEmpty(data.address) ||
        utils.isEmpty(data.position) ||
        utils.isEmpty(data.documentTypeAttachment) || data.documentTypeAttachment == 'undefined' ||
        utils.isEmpty(data.userId) ||
        Object.keys(files).length === 0) {
        return res.status(400).json({success: false, error: "Datos no válidos"});
    }

    console.log(files)
    let countFiles = Object.keys(files).length;
    let _files = [];
    let attachments = [];

    for (let i = 1; i <= countFiles; i++) {
        _files.push({index: i});
    }

    let isValid = true;
    let message = "";
    for await (file of _files) {
        if (files['file' + file.index].size === 0 || files['file' + file.index].size > appConstants.TAM_MAX_FILE) {
            isValid = false;
            message += ((message.length > 0) ? ", " : "") + `El Archivo ${file.index} con tamaño no válido`;
            break;
        }
        if (!typeFiles.includes(files['file' + file.index].type)) {//if(files['file' + file.index].type != "application/pdf") {
            isValid = false;
            message += ((message.length > 0) ? ", " : "") + `El Archivo ${file.index} sólo en formato PDF, JPEG, JPG, PNG o BMP`;
            break;
        }
        const signedFile = fs.readFileSync(files['file' + file.index].path);
        if (!validatebyteFile(files['file' + file.index].type, signedFile)) {
            isValid = false;
            message += ((message.length > 0) ? ", " : "") + `El Archivo ${file.index} está dañado o no es válido`;
        }
    }

    if (!isValid) return res.status(400).json({success: false, error: message});

    for await (file of _files) {
        file.file = await utils.copyFile(
            files['file' + file.index].path,
            appConstants.PATH_BOX,
            files['file' + file.index].name,
            data.doc,
            Date.now(),
            false,
            false);
        attachments.push(file.file);
    }

    let response = await representativeService.save(data, attachments, false);

    return res.status(200).json(response);
}

const validatebyteFile = (typeFile, signedFile) => {
    switch (typeFile) {
        case "application/pdf":
            return (Buffer.isBuffer(signedFile) && signedFile.lastIndexOf("%PDF-") === 0 && signedFile.lastIndexOf("%%EOF") > -1);
        case "image/jpg":
        case "image/jpeg":
            return (/^(ffd8ffe([0-9]|[a-f]){1}$)/g).test(signedFile.toString('hex').substring(0, 8));
        case "image/png":
            return signedFile.toString('hex').startsWith("89504e47");
        case "image/bmp":
        case "image/x-ms-bmp":
            return signedFile.toString('hex').startsWith("424d");
        default:
            return false;
    }
}


module.exports = {save};
