const reporteService = require('../services/reportService');
const logger = require('../server/logger').logger;
const reporteCasillas = async (req, res, next) => {

    try {
        let usuarioRegistro = req.user.name + ' ' + req.user.lastname;
        const result = await reporteService.reporteCasillas(usuarioRegistro,req.query.fechaInicio || null, req.query.fechaFin || null);
        if (result) {
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.setHeader('Content-Disposition', 'attachment; filename=reporteCasillas.xlsx');
            res.send(result);
            return res;
        }
        return res.sendStatus(400);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(400);
    }
    
}
const reporteNotificaciones = async (req, res, next) => {

    try {
        let usuarioRegistro = req.user.name + ' ' + req.user.lastname;
        const result = await reporteService.reporteNotificaciones(usuarioRegistro,req.query.fechaInicio || null, req.query.fechaFin || null);
        if (result) {
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.setHeader('Content-Disposition', 'attachment; filename=reporteCasillas.xlsx');
            res.send(result);
            return res;
        }
        return res.sendStatus(400);
    } catch (e) {
        logger.error(e);
        return res.sendStatus(400);
    }
    
}
module.exports = {reporteCasillas,reporteNotificaciones};